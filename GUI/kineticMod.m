function varargout = kineticMod(varargin)
% KINETICMOD MATLAB code for kineticMod.fig
%      KINETICMOD, by itself, creates a new KINETICMOD or raises the existing
%      singleton*.
%
%      H = KINETICMOD returns the handle to a new KINETICMOD or the handle to
%      the existing singleton*.
%
%      KINETICMOD('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in KINETICMOD.M with the given input arguments.
%
%      KINETICMOD('Property','Value',...) creates a new KINETICMOD or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before kineticMod_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to kineticMod_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help kineticMod

% Last Modified by GUIDE v2.5 07-Apr-2020 17:17:12

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @kineticMod_OpeningFcn, ...
                   'gui_OutputFcn',  @kineticMod_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before kineticMod is made visible.
function kineticMod_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to kineticMod (see VARARGIN)

% Choose default command line output for kineticMod
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes kineticMod wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = kineticMod_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function pathAIF_Callback(hObject, eventdata, handles)
% hObject    handle to pathAIF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pathAIF as text
%        str2double(get(hObject,'String')) returns contents of pathAIF as a double


% --- Executes during object creation, after setting all properties.
function pathAIF_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pathAIF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function pathTAC_Callback(hObject, eventdata, handles)
% hObject    handle to pathTAC (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pathTAC as text
%        str2double(get(hObject,'String')) returns contents of pathTAC as a double


% --- Executes during object creation, after setting all properties.
function pathTAC_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pathTAC (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in modele.
function modele_Callback(hObject, eventdata, handles)
% hObject    handle to modele (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns modele contents as cell array
%        contents{get(hObject,'Value')} returns selected item from modele
modelType = get(handles.modele, 'Value');

if modelType==3
    set(handles.guessK3, 'Enable', 'off');
    set(handles.lb3, 'Enable', 'off');
    set(handles.ub3, 'Enable', 'off');
    set(handles.fitK3,'Enable', 'off');
    set(handles.calculatedK3,'Enable', 'off');
else
    set(handles.guessK3, 'Enable', 'on');
    set(handles.lb3, 'Enable', 'on');
    set(handles.ub3, 'Enable', 'on');
    set(handles.fitK3,'Enable', 'on');
    set(handles.calculatedK3,'Enable', 'on');
end



% --- Executes during object creation, after setting all properties.
function modele_CreateFcn(hObject, eventdata, handles)
% hObject    handle to modele (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in chooseAIF.
function chooseAIF_Callback(hObject, eventdata, handles)
% hObject    handle to chooseAIF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Loads text file with AIF data and displays path in the pathAIF box
[aifFile, aifPath] = uigetfile('*.*', 'Select blood data');
aif=[aifPath aifFile];
set(handles.pathAIF, 'string', aif);
fileID = fopen(aif);
data = textscan(fileID, '%f %f');
fclose(fileID);
timeBlood = data{1}; aifData = data{2};
% Clean up the array
aifData(isnan(timeBlood))=[]; timeBlood(isnan(aifData))=[]; timeBlood(isnan(timeBlood))=[]; 
aifData(isnan(aifData))=[];

%Ensure time is in minutes
if timeBlood(end)>=60
    answer = questdlg('Is time data in seconds or minutes?', 'Specify time unit','Seconds','Minutes', 'Other', 'Seconds');
    switch answer
        case 'Seconds'
            timeBlood = timeBlood/60;
        case 'Other'
            prompt = {'Enter conversion factor (e.g. seconds to minutes = 60'};
            dlgtitle = 'Input';
            dims = [1 35];
            definput = {'60'};
            convFactor = inputdlg(prompt,dlgtitle,dims,definput);
            timeBlood = timeBlood/str2double(convFactor);
    end
end
% Set initial guess for AIF fit
A=max(aifData)/3;
set(handles.A, 'string', num2str(A));

%Assign AIF data to handles
handles.timeBlood = timeBlood;
handles.blood = aifData;
guidata( hObject, handles);





% --- Executes on button press in choseTAC.
function choseTAC_Callback(hObject, eventdata, handles)
% hObject    handle to choseTAC (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[tacFile, tacPath] = uigetfile('*.*', 'Select tissue data');
tac=[tacPath tacFile];
handles.tacName=tacFile;
set(handles.pathTAC, 'string', tac);
fileID = fopen(tac);
data = textscan(fileID, '%f %f');
fclose(fileID);
time = data{1}; tacData = data{2};
% Clean up the array
tacData(isnan(time))=[]; time(isnan(tacData))=[]; time(isnan(time))=[]; 
tacData(isnan(tacData))=[];

%Ensure time is in minutes
if time(end)>=60
    answer = questdlg('Is time data in seconds or minutes?', 'Specify time unit','Seconds','Minutes', 'Other', 'Seconds');
    switch answer
        case 'Seconds'
            time = time/60;
        case 'Other'
            prompt = {'Enter conversion factor (e.g. seconds to minutes = 60'};
            dlgtitle = 'Input';
            dims = [1 35];
            definput = {'60'};
            convFactor = inputdlg(prompt,dlgtitle,dims,definput);
            time = time/str2double(convFactor);
    end
end

%Assign AIF data to handles
handles.time = time;
handles.tac = tacData;
guidata( hObject, handles);



function guessK1_Callback(hObject, eventdata, handles)
% hObject    handle to guessK1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of guessK1 as text
%        str2double(get(hObject,'String')) returns contents of guessK1 as a double


% --- Executes during object creation, after setting all properties.
function guessK1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to guessK1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lb1_Callback(hObject, eventdata, handles)
% hObject    handle to lb1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of lb1 as text
%        str2double(get(hObject,'String')) returns contents of lb1 as a double


% --- Executes during object creation, after setting all properties.
function lb1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lb1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ub1_Callback(hObject, eventdata, handles)
% hObject    handle to ub1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ub1 as text
%        str2double(get(hObject,'String')) returns contents of ub1 as a double


% --- Executes during object creation, after setting all properties.
function ub1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ub1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function calculatedK1_Callback(hObject, eventdata, handles)
% hObject    handle to calculatedK1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of calculatedK1 as text
%        str2double(get(hObject,'String')) returns contents of calculatedK1 as a double


% --- Executes during object creation, after setting all properties.
function calculatedK1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to calculatedK1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in fitK1.
function fitK1_Callback(hObject, eventdata, handles)
% hObject    handle to fitK1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of fitK1



function guessK2_Callback(hObject, eventdata, handles)
% hObject    handle to guessK2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of guessK2 as text
%        str2double(get(hObject,'String')) returns contents of guessK2 as a double


% --- Executes during object creation, after setting all properties.
function guessK2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to guessK2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lb2_Callback(hObject, eventdata, handles)
% hObject    handle to lb2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of lb2 as text
%        str2double(get(hObject,'String')) returns contents of lb2 as a double


% --- Executes during object creation, after setting all properties.
function lb2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lb2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ub2_Callback(hObject, eventdata, handles)
% hObject    handle to ub2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ub2 as text
%        str2double(get(hObject,'String')) returns contents of ub2 as a double


% --- Executes during object creation, after setting all properties.
function ub2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ub2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function calculatedK2_Callback(hObject, eventdata, handles)
% hObject    handle to calculatedK2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of calculatedK2 as text
%        str2double(get(hObject,'String')) returns contents of calculatedK2 as a double


% --- Executes during object creation, after setting all properties.
function calculatedK2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to calculatedK2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in fitK2.
function fitK2_Callback(hObject, eventdata, handles)
% hObject    handle to fitK2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of fitK2



function guessK3_Callback(hObject, eventdata, handles)
% hObject    handle to guessK3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of guessK3 as text
%        str2double(get(hObject,'String')) returns contents of guessK3 as a double


% --- Executes during object creation, after setting all properties.
function guessK3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to guessK3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lb3_Callback(hObject, eventdata, handles)
% hObject    handle to lb3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of lb3 as text
%        str2double(get(hObject,'String')) returns contents of lb3 as a double


% --- Executes during object creation, after setting all properties.
function lb3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lb3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ub3_Callback(hObject, eventdata, handles)
% hObject    handle to ub3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ub3 as text
%        str2double(get(hObject,'String')) returns contents of ub3 as a double


% --- Executes during object creation, after setting all properties.
function ub3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ub3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function calculatedK3_Callback(hObject, eventdata, handles)
% hObject    handle to calculatedK3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of calculatedK3 as text
%        str2double(get(hObject,'String')) returns contents of calculatedK3 as a double


% --- Executes during object creation, after setting all properties.
function calculatedK3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to calculatedK3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in fitK3.
function fitK3_Callback(hObject, eventdata, handles)
% hObject    handle to fitK3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of fitK3



function guessVb_Callback(hObject, eventdata, handles)
% hObject    handle to guessVb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of guessVb as text
%        str2double(get(hObject,'String')) returns contents of guessVb as a double


% --- Executes during object creation, after setting all properties.
function guessVb_CreateFcn(hObject, eventdata, handles)
% hObject    handle to guessVb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lb4_Callback(hObject, eventdata, handles)
% hObject    handle to lb4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of lb4 as text
%        str2double(get(hObject,'String')) returns contents of lb4 as a double


% --- Executes during object creation, after setting all properties.
function lb4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lb4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ub4_Callback(hObject, eventdata, handles)
% hObject    handle to ub4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ub4 as text
%        str2double(get(hObject,'String')) returns contents of ub4 as a double


% --- Executes during object creation, after setting all properties.
function ub4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ub4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function calculatedVb_Callback(hObject, eventdata, handles)
% hObject    handle to calculatedVb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of calculatedVb as text
%        str2double(get(hObject,'String')) returns contents of calculatedVb as a double


% --- Executes during object creation, after setting all properties.
function calculatedVb_CreateFcn(hObject, eventdata, handles)
% hObject    handle to calculatedVb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in fitVb.
function fitVb_Callback(hObject, eventdata, handles)
% hObject    handle to fitVb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of fitVb


% --- Executes on button press in noMetaboliteCorr.
function noMetaboliteCorr_Callback(hObject, eventdata, handles)
% hObject    handle to noMetaboliteCorr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of noMetaboliteCorr


% --- Executes on button press in analyticMetabCorr.
function analyticMetabCorr_Callback(hObject, eventdata, handles)
% hObject    handle to analyticMetabCorr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of analyticMetabCorr


% --- Executes on button press in measuredMetab.
function measuredMetab_Callback(hObject, eventdata, handles)
% hObject    handle to measuredMetab (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of measuredMetab



function a0_Callback(hObject, eventdata, handles)
% hObject    handle to a0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of a0 as text
%        str2double(get(hObject,'String')) returns contents of a0 as a double


% --- Executes during object creation, after setting all properties.
function a0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to a0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function pathPlasma_Callback(hObject, eventdata, handles)
% hObject    handle to pathPlasma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pathPlasma as text
%        str2double(get(hObject,'String')) returns contents of pathPlasma as a double


% --- Executes during object creation, after setting all properties.
function pathPlasma_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pathPlasma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function m_Callback(hObject, eventdata, handles)
% hObject    handle to m (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of m as text
%        str2double(get(hObject,'String')) returns contents of m as a double


% --- Executes during object creation, after setting all properties.
function m_CreateFcn(hObject, eventdata, handles)
% hObject    handle to m (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in measuredPlasma.
function measuredPlasma_Callback(hObject, eventdata, handles)
% hObject    handle to measuredPlasma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[plasmaFile, plasmaPath] = uigetfile('*.*', 'Select plasma data');
plasma = [plasmaPath plasmaFile];
set(handles.pathPlasma, 'String', plasma);
path = get(handles.pathPlasma, 'string');
fileID = fopen(path);
data = textscan(fileID, '%f %f');
fclose(fileID);
timePlasma = data{1}; plasma = data{2};
% Clean up the array
plasma(isnan(timePlasma))=[]; timePlasma(isnan(plasma))=[]; timePlasma(isnan(timePlasma))=[];
plasma(isnan(plasma))=[];
%Ensure time is in minutes
if timePlasma(end)>=60
    answer = questdlg('Is plasma time data in seconds or minutes?', 'Specify time unit','Seconds','Minutes', 'Other', 'Seconds');
    switch answer
        case 'Seconds'
            timePlasma = timePlasma/60;
        case 'Other'
            prompt = {'Enter conversion factor (e.g. seconds to minutes = 60'};
            dlgtitle = 'Input';
            dims = [1 35];
            definput = {'60'};
            convFactor = inputdlg(prompt,dlgtitle,dims,definput);
            timePlasma = timePlamsa/str2double(convFactor);
    end
end
handles.plasma = plasma;
handles.timePlasma = timePlasma;
guidata( hObject, handles);


function A_Callback(hObject, eventdata, handles)
% hObject    handle to A (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of A as text
%        str2double(get(hObject,'String')) returns contents of A as a double


% --- Executes during object creation, after setting all properties.
function A_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in fitAIF.
function fitAIF_Callback(hObject, eventdata, handles)
% hObject    handle to fitAIF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
A=max(aifData);


% --- Executes on button press in fitTissue.
function fitTissue_Callback(hObject, eventdata, handles)
% hObject    handle to fitTissue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Check that we have tissue file
tissueFile = get(handles.pathTAC, 'String');
if isempty(tissueFile)
    msgbox('Please enter a tissue file')
    return
end

%Check that we have fitted AIF
if ~isfield(handles, 'PCp')
    msgbox('You need to fit the AIF')
    return
end

%Get guesses
guessK1 = str2double(get(handles.guessK1, 'String')); guessK2 = str2double(get(handles.guessK2, 'String'));
guessK3 = str2double(get(handles.guessK3, 'String')); guessVb = str2double(get(handles.guessVb, 'String'));

%Get lower bounds
lb1 = str2double(get(handles.lb1, 'String')); lb2 = str2double(get(handles.lb2, 'String'));
lb3 = str2double(get(handles.lb3, 'String')); lb4 = str2double(get(handles.lb4, 'String'));

%Get upper bounds
ub1 = str2double(get(handles.ub1, 'String')); ub2 = str2double(get(handles.ub2, 'String'));
ub3 = str2double(get(handles.ub3, 'String')); ub4 = str2double(get(handles.ub4, 'String'));

%Check bounds
if (lb1>=ub1 || lb2>=ub2 || lb3>=ub3 || lb4>=ub4)
    msgbox('Lower bounds cannot be greater or equal to upper bounds')
    return
end

if ub4>1
    msgbox('Vb cannot be greater than 1')
    return
end

%Get fixed values
fitK1 = get(handles.fitK1, 'Value'); fitK2 = get(handles.fitK2, 'Value');
fitK3 = get(handles.fitK3, 'Value'); fitVb = get(handles.fitVb, 'Value');

%Get data for fit
time = handles.time;
curve = handles.tac;
PCb = handles.PCb;
PCp = handles.PCp;

% Check if we apply weighting
weightFlag = get(handles.weight, 'Value');

% Interpolate data on uniform scale
[~, Imax] = max(curve);
offset = str2num(get(handles.peakOffset, 'String'));

if weightFlag == 0
    weights=ones(size(time));
else
    weightFactor = str2double(get(handles.weightFactor, 'String'));
    range = str2double(get(handles.range, 'String'));
    weights=ones(size(time));
    weights((Imax+offset-range):(Imax+offset+range)) = weights((Imax+offset-range):(Imax+offset+range)).*weightFactor;   
    if (weightFactor <=0 || range <0)
        msgbox('Wrong weighting parameter')
        return
    end
end

interpBlood = calculateaif(time, PCb); 

modelType = get(handles.modele, 'Value');

switch modelType
    case 1 %Acetate model 7
        modelNum = 7;
        guesses = [guessK1, guessK2, guessK3, guessVb];
        lb = [lb1, lb2, lb3, lb4];
        ub = [ub1, ub2, ub3, ub4];
        order = logical([fitK1, fitK2, fitK3, fitVb]);
        startingParams = guesses(order);
        lb = lb(order);
        ub = ub(order);
        fixedK = guesses(~order);
    case 2 %FDG model 4
        modelNum = 4;
        guesses = [guessK1, guessK2, guessK3, guessVb];
        lb = [lb1, lb2, lb3, lb4];
        ub = [ub1, ub2, ub3, ub4];
        order = logical([fitK1, fitK2, fitK3, fitVb]);
        startingParams = guesses(order);
        lb = lb(order);
        ub = ub(order);
        fixedK = guesses(~order);
    case 3 %Ga-DOTA model 1
        modelNum = 1;
        guesses = [guessK1, guessK2, guessVb];
        lb = [lb1, lb2, lb4];
        ub = [ub1, ub2, ub4];
        order = logical([fitK1, fitK2, fitVb]);
        startingParams = guesses(order);
        lb = lb(order);
        ub = ub(order);
        fixedK = guesses(~order);
end

% Direct weighting with lsqnonlin we don't interpolate data because it
% makes fit less stable and reproducible among MATLAB version
Options = optimoptions('lsqnonlin');
Options = optimoptions(Options,'TolFun',1e-8,'TolX', 1e-8, 'Display', 'final-detailed', 'MaxIter', 2000);
%
F = @(vk)acetateModLsqnonlin(time, vk, fixedK, order, PCp, interpBlood, modelNum, PCb, curve, weights);
[fitk,resnorm] = lsqnonlin(F, startingParams, lb, ub, Options); %For direct weighting
resnorm %Display residual norm
% If you want to try multiple solutions 
%perturbation = 0.1.*startingParams;
% nTry = 1;
% for mag=-5:5
%     currStartingP =  startingParams + (mag.*perturbation);
%     try
%         %      %[fitk,resnorm, ~, flag] = lsqcurvefit(F, startingParams, interpTime, interpCurve', lb, ub, Options);
%         %      [fitk,resnorm, ~, flag] = lsqcurvefit(F, startingParams, time, curve, lb, ub, Options);
%         [fitkTry(nTry,:),resnorm(nTry),~, flag(nTry)] = lsqnonlin(F, currStartingP, lb, ub, Options);
%     catch
%         error('Fit problem')
%     end
%     nTry = nTry+1;
% end
% 
% resnorm(flag <=0)=[];
% fitkTry(flag <=0,:)=[];
% [~,I] = min(resnorm);
% fitk =fitkTry(I,:);

try    k(order)=fitk;
catch
    error('Wrong parameter');
end
k(~order)=fixedK;

switch modelType
    case 1 %Acetate model 7
        set(handles.calculatedK1, 'String', num2str(k(1)));
        set(handles.calculatedK2, 'String', num2str(k(2)));
        set(handles.calculatedK3, 'String', num2str(k(3)));
        set(handles.calculatedVb, 'String', num2str(k(4)));
    case 2 %FDG model 4
        set(handles.calculatedK1, 'String', num2str(k(1)));
        set(handles.calculatedK2, 'String', num2str(k(2)));
        set(handles.calculatedK3, 'String', num2str(k(3)));
        set(handles.calculatedVb, 'String', num2str(k(4)));
    case 3 %Ga-DOTA model 1
        set(handles.calculatedK1, 'String', num2str(k(1)));
        set(handles.calculatedK2, 'String', num2str(k(2)));
        set(handles.calculatedVb, 'String', num2str(k(3)));
end

%Display fit
AIFb = calculateaif(time, PCb);
fit = concModelAcetate(time, k, [], true(size(k)), PCp, AIFb, modelNum, PCb);
handles.fit = fit;
axes(handles.fitTAC);
cla
p = plot(time, curve, time, fit);
p(1).Marker = 'o';
p(1).LineWidth = 1;
p(2).LineWidth = 1;
legend('Blood data', 'Fit')
text(time(Imax+offset),curve(Imax+offset),'Peak');
guidata( hObject, handles);


        

% --- Executes on button press in fitBlood.
function fitBlood_Callback(hObject, eventdata, handles)
% hObject    handle to fitBlood (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Check if we have blood data
bloodFile = get(handles.pathAIF, 'String');
if isempty(bloodFile)
    msgbox('Please enter a plasma file')
    return
end

% Do metabolite correction if needed
h=get(handles.metabolite,'SelectedObject');
metaboliteCorr = get(h,'Tag');
switch metaboliteCorr
    case 'noMetaboliteCorr'
        plasma = handles.blood;
        handles.plasma = plasma;
        handles.timePlasma = handles.timeBlood;
    case 'analyticMetabCorr'
        blood = handles.blood;
        time = handles.timeBlood;
        a0 = str2double(get(handles.a0, 'string'));
        m = str2double(get(handles.m, 'string'));
        plasma = blood -(a0.*blood.*(1-exp(-log(2)/m.*time)));
        handles.plasma = plasma;
        handles.timePlasma = time;
    case 'measuredMetab'
        %Check if we have a plasma file
        plasmaFile = get(handles.pathPlasma, 'String');
        if isempty(plasmaFile)
            msgbox('Please enter a plasma file')
            return
        end
end
       
A = str2double(get(handles.A, 'string'));
PCb = fitaifparametersAcetate(handles.timeBlood, handles.blood, A, 'blood');
PCp = fitaifparametersAcetate(handles.timePlasma, handles.plasma, A, 'blood');

% Plot fits
fittedPlasma = calculateaif(handles.timePlasma, PCp);
axes(handles.AIF);
cla
p = plot(handles.timePlasma, handles.plasma, handles.timePlasma, fittedPlasma);
p(1).Marker = 'o';
p(1).LineWidth = 1;
p(2).LineWidth = 1;
legend('Plasma data', 'Fit')

handles.PCb = PCb;
handles.PCp = PCp;
guidata( hObject, handles);

% Plot TAC
curve = handles.tac;
axes(handles.fitTAC);
cla
p = plot(handles.time, curve);
p(1).Marker = 'o';
p(1).LineWidth = 1;
legend('Blood data')
%text(time(end),min(curve),['RSS ' num2str(resnorm,4)]);


% --- Executes on button press in export.
function export_Callback(hObject, eventdata, handles)
% hObject    handle to export (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Get file
[file,path] = uiputfile('*.csv', 'File to save');

%Prepare info
subjectID = handles.tacName;

modelType = get(handles.modele, 'Value');
time = handles.time;
fit = handles.fit;
headers = {'Time' 'Fit'};
switch modelType
    case 1 %Acetate model 7
        K1 = str2double(get(handles.calculatedK1, 'String'));
        k2 = str2double(get(handles.calculatedK2, 'String'));
        k3 = str2double(get(handles.calculatedK3, 'String'));
        Vb = str2double(get(handles.calculatedVb, 'String'));
        names = {'K1' 'k2' 'k3' 'vb'};
        values = {K1 k2 k3 Vb};
        fid = fopen([path file], 'a');
        fprintf(fid, '\n%s\n', subjectID);
        fprintf(fid, '%s\n', 'Acetate model');
        fprintf(fid, '%s\t', names{1,1:end}) ;    
        fprintf(fid, '\n %f\t %f\t %f\t %f\n',values{1:1:end});     
        fprintf(fid, '%s\t', headers{1,1:end});
        fprintf(fid, '%s\n',[]);
        fprintf(fid, '%f\t %f\n', [time, fit]');
        fclose(fid);
        
    case 2 %FDG model 4
        K1 = str2double(get(handles.calculatedK1, 'String'));
        k2 = str2double(get(handles.calculatedK2, 'String'));
        k3 = str2double(get(handles.calculatedK3, 'String'));
        Vb = str2double(get(handles.calculatedVb, 'String'));
        names={'K1' 'k2' 'k3' 'vb'};
        values = {K1 k2 k3 Vb};
        fid = fopen([path file], 'a');
        fprintf(fid, '\n%s\n', subjectID);
        fprintf(fid, '%s\n', 'FDG model');
        fprintf(fid, '%s\t', names{1,1:end}) ;    
        fprintf(fid, '\n %f\t %f\t %f\t %f\n',values{1:1:end}); 
        fprintf(fid, '%s\t', headers{1,1:end});
        fprintf(fid, '%s\n',[]);
        fprintf(fid, '%f\t %f\n', [time, fit]');
        fclose(fid);
        
    case 3 %Ga-DOTA model 1
        K1 = str2double(get(handles.calculatedK1, 'String'));
        k2 = str2double(get(handles.calculatedK2, 'String'));
        Vb = str2double(get(handles.calculatedVb, 'String'));
        names={'K1' 'k2' 'vb'};
        values = {K1 k2 Vb};
        fid = fopen([path file], 'a');
        fprintf(fid, '\n%s\n', subjectID);
        fprintf(fid, '%s\n', 'Perfusion model');
        fprintf(fid, '%s\t', names{1,1:end}) ;    
        fprintf(fid, '\n %f\t %f\t %f\n', values{1:1:end});  
        fprintf(fid, '%s\t', headers{1,1:end});
        fprintf(fid, '%s\n',[]);
        fprintf(fid, '%f\t %f\n', [time, fit]');
        fclose(fid);
end


% --- Executes on button press in weight.
function weight_Callback(hObject, eventdata, handles)
% hObject    handle to weight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of weight




function weightFactor_Callback(hObject, eventdata, handles)
% hObject    handle to weightFactor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of weightFactor as text
%        str2double(get(hObject,'String')) returns contents of weightFactor as a double


% --- Executes during object creation, after setting all properties.
function weightFactor_CreateFcn(hObject, eventdata, handles)
% hObject    handle to weightFactor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function range_Callback(hObject, eventdata, handles)
% hObject    handle to range (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of range as text
%        str2double(get(hObject,'String')) returns contents of range as a double


% --- Executes during object creation, after setting all properties.
function range_CreateFcn(hObject, eventdata, handles)
% hObject    handle to range (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function peakOffset_Callback(hObject, eventdata, handles)
% hObject    handle to peakOffset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of peakOffset as text
%        str2double(get(hObject,'String')) returns contents of peakOffset as a double


% --- Executes during object creation, after setting all properties.
function peakOffset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to peakOffset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
