function [ P2,resnorm,residual,exitflag,output,lambda,jacobian ] = fitaifparametersAcetate( time, conc, dose, varargin )
% This function fits the AIF parameters P =[ti, tf, A, wd, we, td, te]
% either for blood AIF or reference region (RR) AIF.
%
% Input : 1) Time = uniform time scale
%         2) Conc = corresponding concentratio/TAC
%         3) dose = Injected dose or approximation
%         4) varargin{1} = type of fit ('blood' or 'RR'), 
%            varargin{2} = startingParameters if any
% 
% Output : 1) P2 = AIF parameters 
%          2) lsqucurvefit parameters to asses the quality of the fit
% 
% Created by Marie Anne Richard, based on the work of Vincent Turgeon
% (vt_fitPfromdata) and J�r�mie Fouquet (fq_fitaif)
% v.1.0 June 18, 2014

% Definition of lower (lb) and upper (ub) boudaries as well as fit options.

%Interpolate for uniform weights
interpTime=time(1):1/60:time(end);
interpConc=interp1(time,conc,interpTime);
conc=interpConc';
time=interpTime';

lb = [-10; -10; 1e-6; 1e-6; 1e-6; 1e-6; 5];
ub = [10; 10; 1e2; 1e2; 20; 30; 10e3];

Options = optimset('lsqcurvefit');
Options = optimset(Options,'TolFun',10e-8, 'TolX', 10e-8,'Display', 'off');

% Prefit to find ti and tf
if nargin>4
    sP1=varargin{2};
else
    sP1=[0 1 100 0.03 0.015 2 50 ]; 
end

order=true(1,7);
if strcmp(varargin{1}, 'blood')
    F1 = @(vP, time)calculateaif(time, vP);
elseif strcmp(varargin{1}, 'RR') %Reference region not implemented in the GUI
    F1 = @(vP, time)mar_calculateconcentration(time, vP, varargin{2}, 'P');
else error('Valid options are blood and RR : abort function');
end
P1 = lsqcurvefit(F1, sP1, time, conc, lb, ub, Options);


%Fix the value of A as per J�r�mie Fouquet's equation (A and we, wd are coupled)
A = dose/(P1(2)-P1(1));
order(3)=false;
sP2 = P1(order);% reuse prefit for starting param
sP2(3)=A;
lb2=lb(order);
ub2=ub(order);

if strcmp(varargin{1}, 'blood')
    F2 = @(vP, time)calculateaif(time, vP, A, order);
else strcmp(varargin{1}, 'RR');
    F2 = @(vP, time)mar_calculateconcentration(time, vP, varargin{2}, 'P', A, order);
end
[varP,resnorm,residual,exitflag,output,lambda,jacobian] = lsqcurvefit(F2, sP2, time, conc, lb2, ub2, Options);

P2=zeros(1,numel(order));
P2(order)=varP;
P2(3)=A;



