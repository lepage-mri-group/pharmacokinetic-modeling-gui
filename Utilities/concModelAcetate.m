function [Ctot, C] = concModelAcetate(time, varK, fixedK, order, PCp, AIFb, modelNum, PCb)
% concModelAcetate creates model curves from different kinetic models for
% 11C-acetate.
% Input: 1) time = time vector; 2)varK = variable kinetic paramters;
% 3)fixedK = fixed kinetic parameters;
% 4)order = order: position of variable (true) and fixed (false) paramters %
% 5) PCp = AIF parametes; 6) AIFb = blood concentration (including
% metabolites)
% 7) PCb = whole blood AIF parameters
% metabolites; 7) modelNum = model number

% v 1.0 By MAR, 2018-03-05

% Get complete kinetic parameter array
k=zeros(size(order));
k(order) = varK;
k(~order) = fixedK;

% Select model
switch modelNum
    case 1
        if numel(order)~=3
            error('Wrong number of kinetic parameters for model')
        end
        y0=0;
        [t,C]=ode45(@(t,C)odefun1(t,C,k,PCp), time, y0);
        Ctot = (1-k(3)).*(C(:,1))+(AIFb.*k(3));
        %Ctot=Ctot';
    case 2
        if numel(order)~=5
            error('Wrong number of kinetic parameters for model')
        end
        y0=[0,0];
        [t,C]=ode45(@(t,C)odefun2(t,C,k,PCp), time, y0);
        Ctot = (1-k(5)).*(C(:,1)+C(:,2))+(AIFb.*k(5));
        %Ctot=Ctot';
    case 3
        if numel(order)~=5
            error('Wrong number of kinetic parameters for model')
        end
        y0=[0,0];
        [t,C]=ode45(@(t,C)odefun3(t,C,k,PCp), time, y0);
        Ctot = (1-k(5)).*(C(:,1)+C(:,2))+(AIFb.*k(5));
        %Ctot=Ctot';
    case 4
        if numel(order)~=4
            error('Wrong number of kinetic parameters for model')
        end
        y0=[0,0];
        [t,C]=ode45(@(t,C)odefun4(t,C,k,PCp), time, y0);
        Ctot = (1-k(4)).*(C(:,1)+C(:,2))+(AIFb.*k(4));
        %Ctot=Ctot';
    case 5
        if numel(order)~=6
            error('Wrong number of kinetic parameters for model')
        end
        y0=[0,0,0];
        [t,C]=ode45(@(t,C)odefun5(t,C,k,PCp), time, y0);
        Ctot = (1-k(6)).*(C(:,1)+C(:,2)+C(:,3))+(AIFb.*k(6));
        %Ctot=Ctot';
    case 6
        if numel(order)~=5
            error('Wrong number of kinetic parameters for model')
        end
        y0=[0,0,0];
        [t,C]=ode45(@(t,C)odefun6(t,C,k,PCp), time, y0);
        Ctot = (1-k(5)).*(C(:,1)+C(:,2)+C(:,3))+(AIFb.*k(5));
        %Ctot=Ctot';
    case 7
        if numel(order)~=4
            error('Wrong number of kinetic parameters for model')
        end
        y0=[0,0,0];
        [t,C]=ode45(@(t,C)odefun7(t,C,k,PCp), time, y0);
        Ctot = (1-k(4)).*(C(:,1)+C(:,2)+C(:,3))+(AIFb.*k(4));
        %Ctot=Ctot';
    case 8
        if numel(order)~=5
            error('Wrong number of kinetic parameters for model')
        end
        y0=[0,0];
        [t,C]=ode45(@(t,C)odefun8(t,C,k,PCp,PCb), time, y0);
        Ctot = (1-k(5)).*(C(:,1)+C(:,2))+(AIFb.*k(5));
    case 9
        if numel(order)~=6
            error('Wrong number of kinetic parameters for model')
        end
        y0=[0,0,0];
        [t,C]=ode45(@(t,C)odefun9(t,C,k,PCp,PCb), time, y0);
        Ctot = (1-k(6)).*(C(:,1)+C(:,2)+C(:,3))+(AIFb.*k(6));
    otherwise
        error('Specified model does not exist')
end
%Plot results
% figure
% plot(t, Ctot)
 end

% ODE solver functions
function dydt=odefun1(t, C, k, PCp)

Cp = calculateaif(t, PCp); % We need the AIF at time t

dydt=zeros(1,1);
dydt(1) = k(1)*Cp - (k(2)*C(1));

end

function dydt=odefun2(t, C, k, PCp)

Cp = calculateaif(t, PCp); % We need the AIF at time t

dydt=zeros(2,1);

dydt(1) = k(1)*Cp -(k(2)+k(3))*C(1) + k(4)*C(2);
dydt(2) = k(3)*C(1) - k(4)*C(2);

end

function dydt=odefun3(t, C, k, PCp)

Cp = calculateaif(t, PCp); % We need the AIF at time t

dydt=zeros(2,1);

dydt(1) = k(1)*Cp -(k(2)+k(3))*C(1);
dydt(2) = k(3)*C(1) - k(4)*C(2);

end

function dydt=odefun4(t, C, k, PCp)

Cp = calculateaif(t, PCp); % We need the AIF at time t

dydt=zeros(2,1);

dydt(1) = k(1)*Cp -(k(2)+k(3))*C(1);
dydt(2) = k(3)*C(1);

end

function dydt=odefun5(t, C, k, PCp)

Cp = calculateaif(t, PCp); % We need the AIF at time t

dydt=zeros(3,1);

dydt(1) = k(1)*Cp - (k(2)+k(3)+k(4))*C(1) + k(5)*C(2);
dydt(2) = k(4)*C(1) - k(5)*C(2);
dydt(3) = k(3)*C(1) - k(3)*C(3);

end

function dydt=odefun6(t, C, k, PCp)

Cp = calculateaif(t, PCp); % We need the AIF at time t

dydt=zeros(3,1);

dydt(1) = k(1)*Cp - (k(2)+k(3)+k(4))*C(1);
dydt(2) = k(4)*C(1);
dydt(3) = k(3)*C(1) - k(3)*C(3);

end

function dydt=odefun7(t, C, k, PCp)

Cp = calculateaif(t, PCp); % We need the AIF at time t

dydt=zeros(3,1);

dydt(1) = k(1)*Cp - (k(2)+k(3))*C(1);
dydt(2) = k(3)*C(1);
dydt(3) = k(2)*C(1) - k(2)*C(3);

end

function dydt=odefun8(t, C, k, PCp, PCb)

Cp = calculateaif(t, PCp); % We need the plasma acetate at time t
Cb = calculateaif(t, PCb); % We need the blood activity at time t

Cmetab = Cb-Cp; %Metabolites at time t

%Cmetab=PCb(1)*(1-exp(-t*PCb(2))); %Analytic metabolite curve

dydt=zeros(2,1);

dydt(1) = k(1)*Cp - k(2)*C(1);
dydt(2) = k(3)*Cmetab + k(2)*C(1) - k(4)*C(2);

end

function dydt=odefun9(t, C, k, PCp, PCb)

Cp = calculateaif(t, PCp); % We need the plasma acetate at time t
Cb = calculateaif(t, PCb); % We need the blood activity at time t

Cmetab = Cb-Cp; %Metabolites at time t
%Cmetab=PCb(1)*(1-exp(-t*PCb(2))); %Analytic metabolite curve

dydt=zeros(3,1);

dydt(1) = k(1)*Cp - (k(2)+k(3))*C(1);
dydt(2) = k(3)*C(1);
dydt(3) = k(4)*Cmetab - k(5)*C(3);

end
