
function [AIF] = calculateaif(time, varargin)
% This function calculates an analytical AIF based on the fitted
% parameters P = [ti, tf, A, wd, we, td, te] and the time vector. It uses
% the analytical form of the convolution for the AIF model.
%
% Input : 1)time = time vector of the model AIF
%         2) varargin (size 1 or 3) varargin{1} = variable parameters of P (e.g. P=[0 1...], 
%         varargin{2} = fixed parameters of P (usually the value of A), 
%         varargin{3} (flag array) = position of the variable parameters (1) and the fixed
%         ones (0) (e.g. [1 1 0 1 1 1 1];
%
% Created by Marie Anne Richard, based on the work of Vincent Turgeon
% (calculateaif.m) and J�r�mie Fouquet (fq_aiffunc.m)
% v.1.1 July 16, 2014


    % Define full P array
    if numel(varargin)>1
        P = zeros(1,numel(varargin{3}));
        P(varargin{3})=varargin{1};
        P(~varargin{3})=varargin{2};
    else
        P=varargin{1};
    end
    
    if P(1)>=P(2) % Needed only if lower and upper bounds of fit allow ti>tf
        AIF=zeros(numel(time),1);
        return
    end
    
    % Heaviside is problematic if there are Inf values before ti (happens with
    % times<0) because 0*Inf=NAN
    % So, to do convolution by part, we split the time vector

    t_bti = time(time<P(1));
    t_btf = time(time>=P(1) & time <P(2));
    t_atf = time(time>=P(2));
    
    % Convolution by parts
    AIF_btf = P(3)*(P(4)*P(6)*(1-exp(-(t_btf-P(1))/P(6)))+P(5)*P(7)*...
        (1-exp(-(t_btf-P(1))/P(7))));
    AIF_atf = P(3)*(P(4)*P(6)*exp(-(t_atf-P(1))/P(6))*(exp((P(2)-P(1))/P(6))-1)...
        +P(5)*P(7)*exp(-(t_atf-P(1))/P(7))*(exp((P(2)-P(1))/P(7))-1));
    
    if ~isempty(t_bti) % the empty array will not concatenate
        AIF_bti = zeros(numel(t_bti),1); % Before ti AIF = 0
        AIF = vertcat(AIF_bti, AIF_btf, AIF_atf);
    else
        AIF = vertcat(AIF_btf, AIF_atf);
    end




