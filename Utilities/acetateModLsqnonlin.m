function err = acetateModLsqnonlin(time, varK, fixedK, order, PCp, AIFb, modelNum, PCb, ydata, weights)
%Calculates error from acetate model for lsqnonlin with weighting
% Input: 1) ydata : curve to fit; 2) time = time vector; 3)varK = variable kinetic paramters;
% 4)fixedK = fixed kinetic parameters;
% 5)order = order: position of variable (true) and fixed (false) paramters %
% 6) PCp = AIF parametes; 7) AIFb = blood concentration (including
% metabolites)
% 8) PCb = whole blood AIF parameters; 9)weights : weigthing of error

curve = concModelAcetate(time, varK, fixedK, order, PCp, AIFb, modelNum, PCb);

err = curve - ydata;

err = err.*weights;
end

