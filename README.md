# Pharmacokinetic modeling GUI

Pharmacokinetic models for 18F-FDG, 11C-acetate and DCE-MRI.

This GUI was designed in MATLAB R2019a

The 11C-acetate model is taken from: M.A. Richard, D.P. Blondin, C. Noll, R. Lebel, M. Lepage, A.C. Carpentier. Determination of a pharmacokinetic model for [11C]-acetate in brown adipose tissue. EJNMMI Res. 2019 Mar 27;9(1):31.

**How to use**
1) Add folder with subfolder to MATLAB path
2) In the Command Window, type kineticMod
3) In GUI, load blood data and tissue data from .txt files
4) Select a method for metabolite correction
5) Fit blood data ("A" is a variable parameter to modify blood fit). 
   The AIF model is a boxcar of amplitude "A" convoluted with two exponential decay.
5) Fit tissue data. Assess quality with output message in the Command Window.
6) To apply a weighting to the peak value (for better peak fitting): select "Give weight to peak". 
   weight factor = strength of weighting, range = how many points around the peak are affected, 
   Peak offset = to displace the peak
   *This is not used routinely for rat or human data. *
6) Export data in CSV file

to report bugs: gabriel.richard3@usherbrooke.ca